import os
import sys
import time
from time import sleep
import serial
import serial.tools.list_ports
import progressbar

#Global variables:
SLEEPTIMER = 0.2 #seconds
SLEEPTIMERLONG = 1 #second(s)
global userDevice

##Finner porten man skal bruke automatisk
#Og oppretter connection
#Retunerer: ser-objekt (selve koblingen)
def establishConnection():
    print('Search...')
    ports = serial.tools.list_ports.comports(include_links=False)
    for port in ports :
        print('Find port '+ port.device)

    ser = serial.Serial(port.device)
    if ser.isOpen():
        ser.close()

    ser = serial.Serial(port.device, 9600, timeout=1)
    ser.flushInput()
    ser.flushOutput()
    print('Connect ' + ser.name)
    return ser

def writeConfigToDevice(ser):

    dir = os.listdir()
    path = os.path
    #Building list of files:
    possibleFiles = []
    for f in dir:
        if path.isfile(f):
            possibleFiles.append(f)

    print("\nPlease choose correct config file: ")
    for count, item in enumerate(possibleFiles,start=1):
        print("\n\t[" + str(count) + "]\t" + item)
    #Validating inputs:
    while True:
        try:
            number = int(input("\nNumber(1-"+str(len(possibleFiles))+ "): "))
        except:
            print("Not a valid input..")
            continue
        if number not in range(1,len(possibleFiles)):
            print("Input not in range..")
            continue
        else:
            chosenFile = possibleFiles[number-1]
            print("Chosen file: " + chosenFile)
            f = open(chosenFile,"r")
            configFile = list(f) #converting to a list
            break

    configFileSize = len(configFile)
    #Writing to device and shows progressbar:
    bar = progressbar.ProgressBar(max_value=configFileSize)
    for count, line in enumerate(configFile,start=1):
        ser.write(line.encode())
        bar.update(count)
        sleep(SLEEPTIMER)


def setDeviceInConfT(ser):
    ser.write(b'end\n')
    sleep(SLEEPTIMER)
    ser.write(b'conf t\n')


def setDeviceInGlobal(ser):
    ser.write(b'end\n')
    sleep(SLEEPTIMER)


def deleteConfDeviceSwitch(ser):
    #setDeviceInGlobal(ser)
    ser.write(b'erase startup-config\n')
    sleep(SLEEPTIMERLONG)
    ser.write(b'\n')
    sleep(SLEEPTIMERLONG)
    if userDevice == 'S':
        ser.write(b'delete vlan.dat')
        sleep(SLEEPTIMERLONG)
        ser.write(b'\n')
        ser.write(b'\n')
    sleep(SLEEPTIMERLONG)
    ser.write(b'\n')
    ser.write(b'reload\n') #reload tar ca. 2.25 min
    sleep(SLEEPTIMERLONG)
    ser.write(b'no\n')
    ser.write(b'\n')

    print("Device is reloading... ETA: 135 sec...\n PLEASE WAIT")
    for i in progressbar.progressbar(range(145)): #sleeps in 145 seconds
        time.sleep(1)
    ser.write(b'\n')
    sleep(SLEEPTIMERLONG)
    ser.write(b'no\n')
    print("DONE")

#deleteConfDeviceSwitch()
#writeConfigToDevice()

def menyUserChoice():
    menyChoices = ['1','2','3','Q']
    while True:
        print('''

            [1] BACKUP CONFIG FROM DEVICE **
            [2] WRITE CONFIG TO DEVICE
            [3] CLEAN DEVICE
            [Q] QUIT PROGRAM
        ''')
        userChoice = input("\n\n\tChoice (1-3): ")
        if userChoice in menyChoices:
            return userChoice
        else:
            print("Invalid input..")
            continue

def meny():
    global userDevice
    print('''
    authors: Erik Sørli && Aksel Skaar Leirvaag
    created: autumn 2019
  _____   _____ ___  ____ ___
 |  __ \ / ____|__ \|___ \__  \\
 | |__) | (___    ) | __) | ) |
 |  _  / \___ \  / / |__ < / /
 | | \ \ ____) |/ /_ ___) / /_
 |_|__\_\_____/|____|____/____| ____
 |__   __/ __ \  | |  | |/ ____|  _ \\
    | | | |  | | | |  | | (___ | |_) |
    | | | |  | | | |  | |\___ \|  _ <
    | | | |__| | | |__| |____) | |_) |
    |_|  \____/   \____/|_____/|____/

    YOUR SOLTION TO BE PRODUCTIVE IN CISCO NETWORKING
    ''')
    while True:
        print("Choose device (Router or Switch): ")
        userDevice = input("\n\tDevice(R||S): ")
        if userDevice == 'R' or userDevice == 'S':
            break
        else:
            print("ERROR: invalid input..")
            continue
    #CONNECTING TO DEVICE:
    ser = establishConnection()

    while True:
        userChoice = menyUserChoice()
        if userChoice == '1':
            print("ERIK SIN FUNKSJON -- IKKE FERDIG")
            continue
        elif userChoice == '2':
            writeConfigToDevice(ser)
            continue
        elif userChoice == '3':
            deleteConfDeviceSwitch(ser)
            continue
        elif userChoice == 'Q':
            print("Exiting program...")
            sys.exit()
        else:
            print("ERROR: not able to process user choice..")
            sys.exit()



meny()


#TODO: skrive funksjon for devices med passord..
#TODO: erase for router
#TODO: finne ut hvordan sette device i riktig modus (Global/conf t)
